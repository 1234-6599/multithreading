package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CollectorExample {

	public static void main(String[] args) {
		
		List<Integer> numbers=Arrays.asList(10,12,15,20,25,30,33,39,90,95);
		
		List<Integer> oddNumbers=numbers.stream().filter((number)->number%2!=0).collect(Collectors.toList());

		System.out.println(numbers);
		System.out.println(oddNumbers);
	}

}
