package streams;

import java.util.stream.Stream;

public class StreamExampleOne {

	public static void main(String[] args) {
		
		Stream<String> stream=Stream.of("indore","delhi","mumbai","chennai","dewas","dehradoon");
		
		stream.filter((city)->city.startsWith("d")).filter((city)->city.length()==5).forEach(System.out::println);
	
	}

}
