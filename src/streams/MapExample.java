package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class MapExample {

	public static void main(String[] args) {
		
		List<Integer> numbers=Arrays.asList(10,20,30,40,50,60,70,80,90);
		//numbers.stream().map((number)->number+5).forEach(System.out::println);
		List<Integer> increasedNumbers=numbers.stream().map((num)->{return num+5;}).collect(Collectors.toList());
		//numbers.stream().forEach((number)->System.out.println(number+5));

		System.out.println(numbers);
		System.out.println(increasedNumbers);
	}

}
