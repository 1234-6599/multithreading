package streams;

public class Cylinder {
	private double radius, height;
	
	public Cylinder() {}
	public Cylinder(double radius, double height) {
		this.radius=radius; this.height=height;
	}
	public double getRadius() {
		return radius;
	}
	public void setRadius(double radius) {
		this.radius = radius;
	}
	public double getHeight() {
		return height;
	}
	public void setHeight(double height) {
		this.height = height;
	}
	@Override
	public String toString() {
		return "Cylinder [radius=" + radius + ", height=" + height + "]";
	}
	
}
