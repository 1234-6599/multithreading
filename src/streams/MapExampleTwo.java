package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class MapExampleTwo {

	public static void main(String[] args) {

		List<Circle> circles = Arrays.asList(new Circle(10), new Circle(20), new Circle(30));

		List<Circle> filteredCircleList=circles.stream().map((circle)->{circle.setRadius(circle.getRadius()/2); return circle;}).filter((circle)->circle.getRadius()>=10).collect(Collectors.toList());
		/*
		// step-1 (get a stream)
		Stream<Circle> circleStream = circles.stream();

		// step-2 (perform intermediate operation)
		Stream<Circle> transformedCircleStream = circleStream.map((circle) -> {
			circle.setRadius(circle.getRadius() / 2);
			return circle;
		});

		// step-3 (perform another intermediate operation (if required))
		Stream<Circle> filteredCircleStream=transformedCircleStream.filter((circle)->circle.getRadius()>=10);
		
		// step-4 (perform terminal operation (printing the stream, collecting it to collection))
		List<Circle> filteredCircleList=filteredCircleStream.collect(Collectors.toList());
		*/
		System.out.println(filteredCircleList);
		
		/*
		 * List<Cylinder> cylinders=circles.stream().map((circle)->{ Cylinder
		 * cylinder=new Cylinder(circle.getRadius(),circle.getRadius()+5); return
		 * cylinder; }).collect(Collectors.toList());
		 */

		/*
		 * List<Cylinder> cylinders=new ArrayList<>();
		 * 
		 * for(Circle circle:circles) { Cylinder cylinder=new Cylinder();
		 * cylinder.setRadius(circle.getRadius());
		 * cylinder.setHeight(circle.getRadius()+5); cylinders.add(cylinder); }
		 */
		//System.out.println(cylinders);

	}

}
