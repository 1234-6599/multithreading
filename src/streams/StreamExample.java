package streams;

import java.util.stream.IntStream;
import java.util.stream.Stream;

public class StreamExample {
	public static void main(String[] args) {

		//just need to find out the square root of numbers between 1 to 50
		
		Stream<Integer> stream=Stream.of(1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20);
		stream.map((number)->Math.sqrt(number)).filter((number)->number>=2.5).forEach(System.out::println);
		
	}
}
