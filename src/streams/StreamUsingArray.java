package streams;

import java.util.stream.Stream;

public class StreamUsingArray {

	public static void main(String[] args) {
		Integer numbers[]= {10,15,22,45,60,90,460};
		Stream<Integer> stream=Stream.of(numbers);
		stream.forEach(System.out::println);
		
	}

}
