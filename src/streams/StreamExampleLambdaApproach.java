package streams;

import java.util.function.Consumer;
import java.util.function.Predicate;
import java.util.stream.Stream;

public class StreamExampleLambdaApproach {
public static void main(String[] args) {
		
		Stream<Integer> stream1=Stream.of(10,20,30,40,50,60,70);
		
		stream1.filter((number)->number%20==0).forEach((number)->System.out.println(number));
		
		/*
		Stream<Integer> stream2=stream1.filter((number)->number%20==0);
		//stream2.forEach((number)->System.out.println(number));
		stream2.forEach(System.out::println);
		System.out.println("End-of-Main");
		*/

	}

	
}
