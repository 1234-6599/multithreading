package streams;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Stream;

public class StreamExampleTwo {

	public static void main(String[] args) {
		
		List<String> cities=Arrays.asList("indore","bhopal","delhi","chennai","ujjain","banglore","dewas");
		
		Stream<String> stream1=cities.stream();
		stream1.filter((city)->city.startsWith("b")).forEach(System.out::println);
		System.out.println("_____________________________________________________________");
		Stream<String> stream2=cities.stream();
		stream2.filter((city)->city.startsWith("d")).forEach(System.out::println);
		
		

	}

}
