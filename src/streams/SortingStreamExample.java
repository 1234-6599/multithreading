package streams;

import java.util.Arrays;
import java.util.List;

public class SortingStreamExample {
	public static void main(String[] args) {

		List<String> cities=Arrays.asList("delhi","agra","chennai","mumbai","pune","banglore");		
		//cities.stream().sorted().forEach(System.out::println);
		cities.stream().sorted((c1,c2)->c1.length()-c2.length()).forEach(System.out::println);
		System.out.println("Original List");
		System.out.println(cities);
	}
}
