interface Account {
	void openAccount();
	void closeAccount();
	default void registerPAN() {
		System.out.println("Something For Account PAN Registration...!");
	}
}
interface CorporateAccount {
	default void registerPAN() {
		System.out.println("Something for PAN Registration Of Corporate Accounts....!");
	}
}

class SavingAccount implements Account {

	@Override
	public void openAccount() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeAccount() {
		// TODO Auto-generated method stub
		
	}
}

class CurrentAccount implements Account, CorporateAccount {

	@Override
	public void openAccount() {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void closeAccount() {
		// TODO Auto-generated method stub
		
	}
	
	public void registerPAN() {
		//System.out.println("Own RegisterPAN of CurrentAccount");
		CorporateAccount.super.registerPAN();
	}
}

public class DefaultMethodExample {
	public static void main(String[] args) {
		SavingAccount account1=new SavingAccount();
		account1.registerPAN();
		
		CurrentAccount account2=new CurrentAccount();
		account2.registerPAN();
	}
}
