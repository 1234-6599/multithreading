package lambda;

public class MultiThreadingUsingLambda {
	public static void main(String[] args) {

		Thread t1 = new Thread(new Runnable() {
			public void run() {
				for (int i = 1; i <= 10; i++) {
					System.out.println("5 x " + i + " = " + (5 * i));
				}
			}
		});
		Thread t2 = new Thread(() -> {
			for (int i = 1; i <= 10; i++) {
				System.out.println("6 x " + i + " = " + (6 * i));
			}
		});
		Thread t3 = new Thread(() -> {
			for (int i = 1; i <= 10; i++) {
				System.out.println("7 x " + i + " = " + (7 * i));
			}
		});

		t1.start();
		t2.start();
		t3.start();
	}
}
