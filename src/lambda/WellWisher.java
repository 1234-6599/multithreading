package lambda;

public class WellWisher {

	public static void wish(Greeting greeting) {
		greeting.greet();
	}

	public static void main(String[] args) {
		wish(new EnglishGreeting());
		wish(new HindiGreeting());
		wish(new FrenchGreeting());
		wish(new Greeting() {
			public void greet() {
				System.out.println("Hello In Russian...!");
			}
		});

		wish(() -> {
			System.out.println("Hello From Lambda");
			System.out.println("Second Line of Greet");
		});

	}

	static class FrenchGreeting implements Greeting {
		@Override
		public void greet() {
			System.out.println("Hellooooianoo");
		}
	}
}
