package lambda;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ProductData {

	public static void main(String[] args) {

		List<Product> products = Arrays.asList(new Product(3343, "mouse", 500), new Product(1235, "Kboard", 1500),
				new Product(6300, "LED", 8000), new Product(7535, "HDD", 2700), new Product(4235, "RAM", 3800));

		Scanner sc = new Scanner(System.in);
		System.out.println("Enter Chocie : 1.  Code,   2. Name,  3. Price ");
		int choice = sc.nextInt();

		switch (choice) {
		case 1:
			Collections.sort(products, (p1, p2) -> p1.getCode() - p2.getCode());
			break;
		case 2:
			Collections.sort(products, (p1, p2) -> p1.getName().compareTo(p2.getName()));
			break;
		case 3:
			Collections.sort(products, (p1, p2) -> p1.getPrice() - p2.getPrice());
			break;
		}

		for (Product product : products) {
			System.out.println(product);
		}

	}

}
