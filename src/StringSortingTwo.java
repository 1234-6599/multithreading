import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

public class StringSortingTwo {
	public static void main(String[] args) {
		List<String> cities = Arrays.asList("indore", "ujjain", "mumbai", "delhi", "goa", "pune", "hyderabad");
		System.out.println(cities);
		Collections.sort(cities, new StringCompInner());
		System.out.println(cities);
	}
	
	
	static class StringCompInner implements Comparator<String>{

		@Override
		public int compare(String s1, String s2) {
			return s1.length()-s2.length();
		}
		
	}
}
