package functional;

import java.util.function.Predicate;

public class PredicateExample {

	public static boolean isEligibleForVoting(Person person, Predicate<Person> predicate) {
		return predicate.test(person);

	}

	public static void main(String[] args) {

		Person p1 = new Person("AB", 20);
		Person p2 = new Person("XY", 15);

		boolean b = isEligibleForVoting(p1, new Predicate<Person>() {

			@Override
			public boolean test(Person p) {
				if (p.getAge() >= 18)
					return true;
				else
					return false;
			}
		});

		boolean b1 = isEligibleForVoting(p2, (person) -> person.getAge() >= 18);
		System.out.println(b);
		System.out.println(b1);
	}

}
